#pragma once
#include <fstream>
#include <string>
#include <iostream>
#include <time.h>
#include<ctime>
#include<cstdlib>
#include<random>
#include "Group.h"
using namespace std;
class Dekanat
{
	friend class Student;
	friend class Group;
private:
	Student **st;
	Group **gr;
	int numSt;
	int numGr;
public:
	Dekanat();
	void addSt();
	void addGr();
	void addMark();
	int bestStudent();
	Group* bestGroup();
	Group* getGroup(int i);
	int getnumSt();
	void moveStudent(int id, Group *gr);
	void delStudent();
	void saveAll();
	void showStudent(int id);
	void showGroup(int name);
	void head();
	//void saveNewStudentsList();

	~Dekanat();
};

