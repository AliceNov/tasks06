#include "Group.h"

Group::Group(string title, Student* head) {
	this->title = title;
	this->head = head;
}

void Group::SetTitle(string title) {
	this->title = title;
}

void Group::SetHead(Student * head) {
	this->head = head;
}

string Group::GetTitle(){
	return title;
}

Student * Group::GetHead() {
	return head;
}

void Group::AddStud(Student * new_student) {
	staff.push_back(new_student);
}

Student * Group::FindStudent(int id) {
	for (int i = 0; i < staff.size(); ++i) {
		if (staff[i]->GetId() == id)
			return staff[i];
	}
	return NULL;
}

void Group::RemoveStudent(Student* stud) {
	for (int i = 0; i < staff.size(); ++i) {
		if (stud == staff[i]) {
			staff[i] = staff.back();
			staff.resize(staff.size() - 1);
			break;
		}
	}
}

double Group::GetAvMark() {
	double average_mark = 0;
	for (int i = 0; i < staff.size(); ++i)
		average_mark += staff[i]->GetAvMark();
	average_mark /= staff.size();
	return average_mark;
}

Group::~Group() {
	for (int i = 0; i < staff.size(); ++i)
		delete[] staff[i];
	staff.clear();
}
